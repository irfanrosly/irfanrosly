import React from "react"
import { Link } from "react-router-dom"

const Header = () => {
	return (
		<div
			style={{
				height: 80,
				display: "flex",
				// justifyContent: "center",
				alignItems: "center",
				paddingLeft: 20
			}}
		>
			<div style={{ flex: 0.5 }}>
				<Link to="/" style={{ textDecoration: "none" }}>
					<h2 style={{ fontSize: 40, color: "#50c878" }}>ToiCode</h2>
				</Link>
			</div>
			<div
				style={{
					display: "flex",
					flex: 0.5,
					justifyContent: "flex-end",
					alignItems: "center",
					height: "100%",
					paddingRight: 50
				}}
			>
				<Link to="/resume" style={{ textDecoration: "none" }}>
					<h2 style={{ fontSize: 20, color: "#50c878", paddingRight: 20 }}>Resume</h2>
				</Link>
				<a href="https://github.com/irfanrosly" style={{ textDecoration: "none" }}>
					<h2 style={{ fontSize: 20, color: "#50c878" }}>Github</h2>
				</a>
			</div>
		</div>
	)
}

export default Header
