import React from "react"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"

import "./App.css"
import Home from "./pages/home"
import NoMatch from "./pages/no-match"
import Header from "./components/header"
import Resume from "./pages/resume"

function App() {
	return (
		<Router>
			<Header />
			<main>
				<Switch>
					<Route exact path="/" component={Home} />
					<Route exact path="/resume" component={Resume} />
					<Route path="/404" component={NoMatch} />
				</Switch>
			</main>
		</Router>
	)
}

export default App
