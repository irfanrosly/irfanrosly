import React from "react"
import styled, { keyframes } from "styled-components"
import { slideInUp, slideInDown, fadeIn } from "react-animations"

const slideUp = keyframes`${slideInUp}`
const slideDown = keyframes`${slideInDown}`
const fade = keyframes`${fadeIn}`

const SlideDownDiv = styled.div`
	animation: 2s ${slideDown};
`

const SlideUpDiv = styled.div`
	animation: 2s ${slideUp};
`

const FadeDiv = styled.div`
	animation: 3s ${fade};
`
const Home = () => {
	return (
		<>
			<div
				style={{
					display: "flex",
					flexDirection: "column",
					height: "50vh",
					alignItems: "center",
					justifyContent: "center"
				}}
			>
				<SlideDownDiv
					style={{
						fontSize: 50
					}}
				>
					Gamers who Coding
				</SlideDownDiv>
				<SlideUpDiv
					style={{
						fontSize: 50
					}}
				>
					Coders who Gaming
				</SlideUpDiv>
			</div>
			<FadeDiv
				style={{ height: 100, alignItems: "center", justifyContent: "center", display: "flex" }}
			>
				<a href="https://steamcommunity.com/id/_awestruck/">
					<img
						alt="steam"
						src={require("../assets/images/steam.png")}
						height="100"
						width="100"
						style={{ marginLeft: 20, marginRight: 20 }}
					/>
				</a>
				<img
					alt="battlenet"
					src={require("../assets/images/battlenet.jpg")}
					height="100"
					width="100"
					style={{ marginLeft: 20, marginRight: 20 }}
				/>
				<a href="https://github.com/irfanrosly">
					<img
						alt="github"
						src={require("../assets/images/github.png")}
						height="100"
						width="100"
						style={{ marginLeft: 20, marginRight: 20 }}
					/>
				</a>
				<a href="https://gitlab.com/irfanrosly">
					<img
						alt="gitlab"
						src={require("../assets/images/gitlab.png")}
						height="100"
						width="100"
						style={{ marginLeft: 20, marginRight: 20 }}
					/>
				</a>
			</FadeDiv>
		</>
	)
}

export default Home
